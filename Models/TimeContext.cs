using Microsoft.EntityFrameworkCore;

namespace TimeApi.Models;

public class TimeContext : DbContext
{
    public TimeContext(DbContextOptions<TimeContext> options)
        : base(options)
    {
    }

    public DbSet<TimeItem> TimeItems { get; set; } = null!;
}