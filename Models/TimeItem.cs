namespace TimeApi.Models;

public class TimeItem
{
    public long Id { get; set; }
    public string? TimeStamp { get; set; }
}