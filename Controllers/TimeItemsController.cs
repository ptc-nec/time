using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TimeApi.Models;

namespace time.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimeItemsController : ControllerBase
    {
        private readonly TimeContext _context;

        public TimeItemsController(TimeContext context)
        {
            _context = context;
        }

        // GET: api/TimeItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TimeItem>>> GetTimeItems()
        {
            return await _context.TimeItems.ToListAsync();
        }

        // GET: api/TimeItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TimeItem>> GetTimeItem(long id)
        {
            var timeItem = await _context.TimeItems.FindAsync(id);

            if (timeItem == null)
            {
                return NotFound();
            }

            return timeItem;
        }

        // PUT: api/TimeItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTimeItem(long id, TimeItem timeItem)
        {
            if (id != timeItem.Id)
            {
                return BadRequest();
            }


            _context.Entry(timeItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TimeItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TimeItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TimeItem>> PostTimeItem(TimeItem timeItem)
        {
            Int64 unixTimestamp = (Int64)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalNanoseconds;
            timeItem.TimeStamp = unixTimestamp.ToString();

            _context.TimeItems.Add(timeItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTimeItem), new { id = timeItem.Id }, timeItem);
        }

        // DELETE: api/TimeItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTimeItem(long id)
        {
            var timeItem = await _context.TimeItems.FindAsync(id);
            if (timeItem == null)
            {
                return NotFound();
            }

            _context.TimeItems.Remove(timeItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TimeItemExists(long id)
        {
            return _context.TimeItems.Any(e => e.Id == id);
        }
    }
}
